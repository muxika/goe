import React from 'react';
import PropTypes from 'prop-types';
import Members from './Members';
import { SectionTitle } from '../styles/styledMembers';

const MemberSection = ({ members, sectionTitle }) => members.length > 0 && (
  <div>
    <SectionTitle>{sectionTitle}</SectionTitle>
    <Members members={members} />
  </div>
);

MemberSection.propTypes = {
  sectionTitle: PropTypes.string.isRequired,
  members: PropTypes.array.isRequired,
};

export default MemberSection;
