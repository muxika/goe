import PropTypes from 'prop-types';
import styled from 'styled-components';
import MemberCard from './MemberCard';

const MembersContainer = styled.div`
  display: flex;
  justify-content: center;
  flex-flow: wrap;

  @media (max-width: 768px){
    flex-direction: column;
    align-items: center;
  }
`

const Members = ({ members }) => (
  <MembersContainer>
    {members && members.map(m => <MemberCard m={m} key={m.sys.id} />)}
  </MembersContainer>
);

Members.propTypes = {
  members: PropTypes.array,
};

export default Members;
