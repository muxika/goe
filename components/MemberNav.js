import Link from 'next/link';
import { useStateValue } from '../state';
import { LinksContainer, MemberPageLink } from '../styles/styledMembers';

const MemberNav = () => {
  const [{ memberView }, ] = useStateValue();

  const isActive = (validation) => {
    const routes = {
      team: memberView === 'team' ? true : false,
      instructorsAndAssistants: memberView === 'instructorsAndAssistants' ? true : false,
      advisoryBoard: memberView === 'advisoryBoard' ? true : false,
      partnersAndSponsors: memberView === 'partnersAndSponsors' ? true : false,
    }

    return routes[validation]
  }

  return (
    <>
      <LinksContainer>
        <li>
          <Link href="/members/team">
            <MemberPageLink active={isActive('team')}>Team</MemberPageLink>
          </Link>
        </li>
        <li>
          <Link href="/members/advisory-board">
            <MemberPageLink active={isActive('executiveBoard')}>Advisory Board</MemberPageLink>
          </Link>
        </li>
        <li>
          <Link href="/members/partners-and-sponsors">
            <MemberPageLink active={isActive('partnersAndSponsors')}>Partners &amp; Sponsors</MemberPageLink>
          </Link>
        </li>
        <li>
          <Link href="/members/instructors-and-assistants">
            <MemberPageLink active={isActive('instructorsAndAssistants')}>Instructors &amp; Assistants</MemberPageLink>
          </Link>
        </li>
      </LinksContainer>
    </>
  );
}

export default MemberNav;
