import PropTypes from 'prop-types';
import { documentToReactComponents } from '@contentful/rich-text-react-renderer';
import { roles, gqlQuery } from '../api/api';
import PostPreview from './PostPreview';
import {
  MemberContainer,
  ProfilePicture,
  MemberBackground,
  Name, PostSectionTitle,
  PostContainer, Role, RoleContainer,
} from '../styles/styledMember';


const Member = ({ m }) => {

  // const memberRoles = roles.filter(r => m[r]).map(r => m[r]).sort((a, b) => a.includes('Instructor') ? 1 : -1)

  return (
    <MemberContainer>
      { m.profilePicture &&
        <ProfilePicture
          src={m.profilePicture.url}
          alt={m.profilePicture.description}
        />
      }
      <Name>{m.name}</Name>
      <RoleContainer>
        {/* {memberRoles.map(r => <Role key={r}>{r}</Role>)} */}
      </RoleContainer>
      <MemberBackground>
        {documentToReactComponents(m.aboutMember.json)}
      </MemberBackground>
      {m.postsCollection.items.length > 0 && (
        <PostContainer>
          <PostSectionTitle>Posts by {m.name}:</PostSectionTitle>
          {m.postsCollection.items.map( 
            post => 
              <PostPreview 
                key={post.sys.id}
                name={m.name}
                post={post}
                fromBlogPage={false} 
              /> 
          )}
        </PostContainer>
      )
      }
    </MemberContainer>
  );
};

// Member.propTypes = {
//   m: PropTypes.object.isRequired,
// };

export default Member;
