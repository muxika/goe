import Link from 'next/link';
import routes from '../api/routes';
import { ContentfulLogo, Foot, FooterContainer, FooterLinkContainer } from '../styles/styledFooter';

const Footer = () => (
  <Foot>
    <FooterContainer>
      <FooterLinkContainer>
        <ul>
          {routes.map(r => 
            <li key={r.label}>
              <Link href={r.path} as={r.as ? r.as : null}>
                <a>{r.label}</a>
              </Link>
            </li>
          )}
        </ul>
      </FooterLinkContainer>
      <a href="https://www.contentful.com/" rel="noreferrer" target="_blank">
        <ContentfulLogo
          src="https://images.ctfassets.net/fo9twyrwpveg/7F5pMEOhJ6Y2WukCa2cYws/398e290725ef2d3b3f0f5a73ae8401d6/PoweredByContentful_DarkBackground.svg"
          alt="Powered by Contentful"
        />
      </a>
      {/* <Copyright>{copyright}</Copyright> */}
      {/* {slogan ? <p>{slogan}</p> : null} */}
    </FooterContainer>
  </Foot>
);

export default Footer;
