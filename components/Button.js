import PropTypes from 'prop-types';
import Link from 'next/link';
import  { ButtonContainer, InnerButton } from '../styles/styledButton'; 

const Button = ({as, buttonText, routeType, url}) => {

  return (
    <>
      {routeType === 'internal' && (
        <Link href={url} as={as ? as : null}>
          <ButtonContainer>
            <InnerButton>
              {buttonText}
            </InnerButton>
          </ButtonContainer>
        </Link>
      )}
      {routeType === 'external' && (
        <ButtonContainer href={url} rel="noreferrer" target="_blank">
          <InnerButton>
            {buttonText}
          </InnerButton>
        </ButtonContainer>
      )}
    </>
  )
}

Button.propTypes = {
  as: PropTypes.string,
  buttonText: PropTypes.string.isRequired,
  routeType: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired
}



export default Button