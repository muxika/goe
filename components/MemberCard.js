import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import { useStateValue } from '../state'
import { Header, InfoContainer, MemberContainer, ProfilePicture, Span } from '../styles/styledMemberCard';
import { formatStringForDynamicRoute } from '../api/api'

const MemberCard = ({ m }) => {
  const { name, profilePicture } = m;
  const [{ memberView }, ] = useStateValue();
  const getRole = () => {
    
    const role = {
      instructorsAndAssistants: m.instructorRole,
      team: m.teamRole,
      partnersAndSponsors: m.partnersAndSponsorsRole,
      executiveBoard: m.executiveBoardRole
    }

    return role[memberView]
  }

  const renderContainer = () => {
    return (
      <>
        <MemberContainer>
          <ProfilePicture
            src={profilePicture.url}
            alt={profilePicture.description}
          />
          <InfoContainer>
            <Header>{name}</Header>
            <Span>{getRole()}</Span>
          </InfoContainer>
        </MemberContainer>
      </>
    )
  }

  return memberView === "instructorsAndAssistants" ? renderContainer() : 
    <Link href="/member/[nom]" as={`/member/${formatStringForDynamicRoute(name)}`}>
      <MemberContainer>
        <ProfilePicture src={profilePicture.url} alt={profilePicture.description} />
        <InfoContainer>
          <Header>{name}</Header>
          <Span>{getRole()}</Span>
        </InfoContainer>
      </MemberContainer>
    </Link>
};

MemberCard.propTypes = {
  m: PropTypes.object.isRequired,
};

export default MemberCard;
