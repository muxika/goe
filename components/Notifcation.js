import { useEffect } from 'react';
import { useStateValue } from '../state'
import { filterPageData, gqlQuery } from '../api/api';
import { NotificationContainer } from '../styles/styledNotification';
import NotificationModal from './NotificationModal';
import NotificationBell from './NotificationBell';
import { NoNotificationIcon } from '../styles/styledNotification';
import alertQuery from '../gql/alertQuery';

const Notification = () => {
  const [{ notification }, dispatch] = useStateValue();
  useEffect(() => {
    gqlQuery(alertQuery).then(({ updatesAlertCollection}) => dispatch({
      notification: updatesAlertCollection.items[0]
    }))
  }, [dispatch]);
  
  return (
    <NotificationContainer>
      {!notification && <NoNotificationIcon />}
      {notification && (    
        <>
          <NotificationBell />
          <NotificationModal notification={notification} />
        </>
      )}
    </NotificationContainer>
  )
}

export default Notification