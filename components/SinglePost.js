import { useEffect } from 'react';
import ReactLoading from 'react-loading';
import { useStateValue, resetNotifications } from '../state'
import { gqlQuery } from '../api/api';
import Post from './Post';

const SinglePost = ({ query, title }) => {
  const variables = { title }
  const [{ post, hasReviewedNotification, loading }, dispatch] = useStateValue();

  useEffect(() => {
    dispatch({
      loading: true
    })
    gqlQuery(query, variables)
      .then(({ blogPostCollection }) => {
        dispatch({
          post: blogPostCollection.items[0],
          ...resetNotifications(hasReviewedNotification),
          loading: false
        })
      })
  }, []);

  return (
    <>
      {
        loading ?
          <ReactLoading type="bubbles" color="#FCBD55" height={667} width={375} />
          : <Post post={post} />
      }
    </>
  )
};

export default SinglePost;


