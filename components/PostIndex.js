import { useEffect, useState } from 'react';
import ReactLoading from 'react-loading';
import styled from 'styled-components';
import { useStateValue, resetNotifications } from '../state'
import { gqlQuery } from '../api/api';
import PostPreview from './PostPreview';

const Container = styled.div`
  display: flex;
  flex-direction: column;
`
const Sort = styled.div`
  background-color: #C0C0C0;
  border-radius: 5px;
  display: inline-flex;
  cursor: pointer;
  color: #333;
  font-family: 'Open Sans', sans-serif;
  margin-top: 24px;
  padding: 12px;
  margin-left: auto;
  margin-right: 8%;
`

const PostIndex = (props) => {
  const { query } = props
  const [sortDirection, setSortDirection] = useState('descending')
  const [{ blog, hasReviewedNotification, loading }, dispatch] = useStateValue();

  useEffect(() => {
    dispatch({ loading: true })
    gqlQuery(query)
      .then(({ blogPostCollection }) =>
        dispatch({
          blog: blogPostCollection.items,
          ...resetNotifications(hasReviewedNotification),
          loading: false
        })
      );
  }, [dispatch]);

  
  const sorted = blog && blog.sort((a, b) => sortDirection === 'descending' ? new Date(b.dateTime) - new Date(a.dateTime) : new Date(a.dateTime) - new Date(b.dateTime)).map(e => <PostPreview key={e.sys.id} post={e} fromBlogPage={true} />)
  
  return (
    <>
      {loading ?
        <ReactLoading type="bubbles" color="#FCBD55" height={667} width={375} />
        : <Container>
            <Sort onClick={() => sortDirection === 'descending' ? setSortDirection('ascending') : setSortDirection('descending')}>
              {sortDirection === 'descending' ? 'Display oldest to newest' : 'Display newest to oldest'}
            </Sort>
            {sorted}
          </Container>
      }
    </>
  );
};

export default PostIndex;