import PropTypes from 'prop-types';
import Link from 'next/link';
import { formatStringForDynamicRoute } from '../api/api';
import { ButtonContainer, PreviewContainer, TagLabel, TitleLink, Name, Description, DetailsContainer, Tag, TagContainer } from '../styles/styledPostPreview'
import Button from './Button';

const PostPreview = ({ post, name, fromBlogPage }) => {
  const { author, description, postBannerImage, tags, title} = post;
  
  return (
    <PreviewContainer postBg={postBannerImage ? postBannerImage.url : undefined} fromBlogPage={fromBlogPage}>
      <DetailsContainer fromBlogPage={fromBlogPage}>
        <Link
          href="/blog/post/[title]"
          as={`/blog/post/${formatStringForDynamicRoute(title)}`}
        >
          <TitleLink fromBlogPage={fromBlogPage}>{title}</TitleLink>
        </Link>
        <Link
          href="/member/[nom]"
          as={fromBlogPage ? `/member/${formatStringForDynamicRoute(author.name)}` : `/member/${formatStringForDynamicRoute(name)}`}
        >
          <Name fromBlogPage={fromBlogPage}> By {fromBlogPage ? author.name : name}</Name>
        </Link>
        <TagContainer fromBlogPage={fromBlogPage}>
          {tags && tags.map(tag => (
            <Tag key={tag}>
              <TagLabel>
                {tag}
              </TagLabel>
            </Tag>
          ))}
        </TagContainer>
        <Link
          href="/blog/post/[title]"
          as={`/blog/post/${formatStringForDynamicRoute(title)}`}
        >
          <Description fromBlogPage={fromBlogPage}>
            {description.substring(0, description.substring(0, 45).slice(-1) === ' ' ? 44 : 45)}...
          </Description>
        </Link>
      </DetailsContainer>
      <ButtonContainer fromBlogPage={fromBlogPage}>
        <Button
          buttonText={`${fromBlogPage ? 'Continue Reading' : 'Read Now'}`}
          routeType="internal"
          url="/blog/post/[title]"
          as={`/blog/post/${formatStringForDynamicRoute(title)}`}
          notification={fromBlogPage}
        />
      </ButtonContainer>
    </PreviewContainer>
  );
};

PostPreview.propTypes = {
  post: PropTypes.object.isRequired,
  fromBlogPage: PropTypes.bool.isRequired
};

export default PostPreview;
