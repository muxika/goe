import { MissionVisionContainer, MissionVisionContent, MissionVisionContentContainer, MissionVisionHeader } from '../styles/styledHome';

const MissionVisionSection = ({ mission, vision }) => (
  <MissionVisionContainer>
      <MissionVisionContentContainer bgColor="#222222">
        <MissionVisionHeader>Our Mission</MissionVisionHeader>
        <MissionVisionContent>
          {mission}
          </MissionVisionContent>
      </MissionVisionContentContainer>
      <MissionVisionContentContainer bgColor="#AD356D">
        <MissionVisionHeader>Our Vision</MissionVisionHeader>
        <MissionVisionContent>
          {vision}
        </MissionVisionContent>
      </MissionVisionContentContainer>
  </MissionVisionContainer>
);

export default MissionVisionSection;
