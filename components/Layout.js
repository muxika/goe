import PropTypes from 'prop-types';
import Head from './Head';
import Nav from './Nav';
import Footer from './Footer';
import { ChildContainer, Container } from '../styles/styledLayout';

const Layout = ({ children }) => {
  return (
    <Container>
      <Head title="Gift of Education" />
      <Nav />
      <ChildContainer>
        {children}
      </ChildContainer>
      <Footer />
    </Container>
  )
}

Layout.propTypes = {
  children: PropTypes.any,
};

export default Layout;
