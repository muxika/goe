import React from 'react';
import PropTypes from 'prop-types';
import MemberNav from './MemberNav';
import { GroupContainer } from '../styles/styledMembers';

const MembersPageWrapper = ({ children }) => (
  <GroupContainer>
    <MemberNav />
    {children}
  </GroupContainer>
);

MembersPageWrapper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default MembersPageWrapper;
