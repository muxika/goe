import React from 'react';
import NextHead from 'next/head';
import { string } from 'prop-types';

const defaultDescription = 'Gift of Education is a professional development program helping teachers develop English language skills and methodologies';
const defaultOGURL = '';
const defaultOGImage = '';

const Head = ({ description, ogImage, title, url }) => (
  <NextHead>
    <meta charSet="UTF-8" />
    <title>{title || ''}</title>
    <link rel="manifest" href="/static/manifest.json" />
    <meta name="description" content={description || defaultDescription} />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <link rel="icon" sizes="192x192" href="/static/GoeLogo.png" />
    <link rel="apple-touch-icon" href="/static/GoeLogo.png" />
    <link rel="mask-icon" href="/static/GoeLogo.svg" color="#49B882" />
    <link rel="icon" href="/static/GoeLogo.png" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600|Oswald:300,400&display=swap" rel="stylesheet" />
    <meta property="og:url" content={url || defaultOGURL} />
    <meta property="og:title" content={title || ''} />
    <meta
      property="og:description"
      content={description || defaultDescription}
    />
    <meta name="twitter:site" content={url || defaultOGURL} />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:image" content={ogImage || defaultOGImage} />
    <meta property="og:image" content={ogImage || defaultOGImage} />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
  </NextHead>
);

Head.propTypes = {
  title: string,
  description: string,
  url: string,
  ogImage: string,
};

export default Head;
