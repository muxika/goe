import { useStateValue } from '../state';
import { ModalContainer, ModalContent, NotificationTitle, MarkAsRead } from '../styles/styledNotification';
import PostPreview from './PostPreview';

const NotificationModal = ({ notification }) => {
  const [{ notificationModalOpen, hasReviewedNotification }, dispatch] = useStateValue();
  const {  contentReference, nonContentRelatedAlert } = notification;
  return (
    <>
      {notificationModalOpen && (
        <ModalContainer>
          <MarkAsRead 
            onClick={() => dispatch(
              { 
                hasReviewedNotification: true,
                notificationModalOpen: false
              }
            )}
            hasReviewedNotification={hasReviewedNotification}
          >
            {hasReviewedNotification ? "You're up to date!" : "Mark as Read" }
          </MarkAsRead>

          {contentReference.postType === 'Blog' && (
            <div onClick={() => dispatch({ hasReviewedNotification: true })}>
              <PostPreview
                key={contentReference.sys.id}
                name={contentReference.author.name}
                post={contentReference}
                fromBlogPage={false}
              />
            </div>
          )}

          {nonContentRelatedAlert && <p>{nonContentRelatedAlert}</p>}
            
        </ModalContainer>
      )}

    </>
  )
}

export default NotificationModal;