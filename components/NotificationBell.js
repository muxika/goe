import { useStateValue } from '../state'
import { NotificationIcon, NoNotificationIcon } from '../styles/styledNotification';

const NotificationBell = () => {
  const [{ hasReviewedNotification, notificationModalOpen }, dispatch] = useStateValue();
  return (
    <>
      {!notificationModalOpen && hasReviewedNotification && (
        <NoNotificationIcon
          onClick={() => dispatch(
            { notificationModalOpen: !notificationModalOpen }
          )}
        />
      )}
      {!notificationModalOpen && !hasReviewedNotification && (
        <NotificationIcon
          onClick={ () => dispatch(
            {notificationModalOpen: !notificationModalOpen}
          )}
        />
      )}
      {notificationModalOpen && !hasReviewedNotification && (
        <NotificationIcon
          onClick={() => dispatch(
            {
              notificationModalOpen: !notificationModalOpen,
              hasReviewedNotification: false
            }
          )}
        />
      )}
      {notificationModalOpen && hasReviewedNotification && (
        <NoNotificationIcon
          onClick={() => dispatch(
            { 
              notificationModalOpen: !notificationModalOpen,
              hasReviewedNotification: true
            }
          )}
        />
      )}
    </>
  )
}

export default NotificationBell