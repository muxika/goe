import ReactMarkdown from 'react-markdown';
import remarkGfm from 'remark-gfm';
import remarkRehype from 'remark-rehype';
import moment from 'moment';
import { ContentContainer, InfoContainer, PostBannerImage } from '../styles/styledBlog';
import { Tag, TagContainer, TagLabel } from '../styles/styledPostPreview';

const Post = ({ post }) => {

  post && console.log(post.body)
  return ( 
    post && <>
      <ContentContainer>
        <h1>{post.title ? post.title : null}</h1>
        <InfoContainer>
          <h4>{post.author.name? post.author.name : null}</h4>
          {post.dateTime && <p>{moment(post.dateTime).format('MMMM Do YYYY')}</p>}
        </InfoContainer>
        <TagContainer>
          {post.tags ? post.tags.map(tag => (
            <Tag key={tag}>
              <TagLabel>
                {tag}
              </TagLabel>
            </Tag>
          )) : null}
        </TagContainer>
        <div>
          {post && <ReactMarkdown children={post.body} remarkPlugins={[remarkGfm, remarkRehype]} />}
        </div>
      </ContentContainer>
    </>
  );
}

export default Post;