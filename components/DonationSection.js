import ReactMarkdown from 'react-markdown';
import { DonationContainer, DonateButton } from '../styles/styledHome';
import Button from './Button';

const DonationSection = ({ appeal, bg, buttonText }) => (
  <DonationContainer bg={bg}>
    <ReactMarkdown source={appeal} />
    <DonateButton>
      <Button buttonText={buttonText} routeType="external" url="https://www.paypal.me/sundasusan" />
    </DonateButton>
  </DonationContainer>
);

export default DonationSection;
