import { HeroContainer, HeroHeader } from '../styles/styledHome';
import Button from './Button';

const HeroSection = ({ buttonText, heroBg, tagline }) => (
  <HeroContainer bg={heroBg}>
    <HeroHeader>{tagline}</HeroHeader>
    <Button buttonText={buttonText} url="/about"  routeType="internal" />
  </HeroContainer>
);

export default HeroSection;
