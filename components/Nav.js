import Link from 'next/link';
import { Slider } from 'react-burgers';
import routes from '../api/routes';
import { useStateValue } from '../state';
import { ButtonContainer, HamburgerContainer, Last, Logo, LogoContainer, MobileLast, MobileLinks, Navbar, NavContainer } from '../styles/styledNav';
import Button from './Button';
import Notification from './Notifcation';

const Nav = () => {
  const [{ hamburgerOpen }, dispatch] = useStateValue();
  return (
    <NavContainer>
      <Navbar>
        <Link href="/">
          <LogoContainer>
            <Logo />
            <a>Gift of Education</a>
          </LogoContainer>
        </Link>
          <ul>
            {routes.map(r => 
              <li key={r.label}>
                <Link href={r.path} as={r.as ? r.as : null} >
                  <a>{r.label}</a>
                </Link>
              </li>
            )}
          </ul>
        <Last>
          <Notification />
          <Button url="https://www.paypal.me/sundasusan" buttonText="Donate Now" routeType="external" />
        </Last>
        <MobileLast>
          <Notification />
          <HamburgerContainer>
            <Slider
              color="#FCBD55"
              onClick={() => dispatch({ hamburgerOpen: !hamburgerOpen, notificationModalOpen: false })}
              active={hamburgerOpen}
            />
          </HamburgerContainer>
        </MobileLast>
      </Navbar>
      {hamburgerOpen && (
        <div>
          <MobileLinks>
            {routes.map(r => 
              <Link href={r.path} as={r.as ? r.as : null} key={r.label}>
                <a onClick={() => dispatch({ hamburgerOpen: false })}>{r.label}</a>
              </Link>
            )}
          </MobileLinks>
          <ButtonContainer>
            <Button url="https://www.paypal.me/sundasusan" buttonText="Donate Now" routeType="external" />
          </ButtonContainer>
          
        </div>
      )}
    </NavContainer>
  )
};

export default Nav;
