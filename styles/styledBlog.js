import styled from 'styled-components';
import { fontSize, lineHeight } from '../api/api';

export const PostBannerImage = styled.img`
  width: 100%;
  height: 400px;
  object-fit: cover;
`
export const ContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: auto auto;
  padding: 48px 0;
  max-width: 80%;
  font-family: 'Open Sans', sans-serif;

  @media(max-width: 550px) {
    padding: 24px;
  }

  div p {
    display: block;
    font-family: 'Open Sans', sans-serif;
    font-size: ${fontSize(14, 18)};
    font-weight: 300;
    letter-spacing: .75px;
    line-height: ${lineHeight()};
    margin: 1em 0;
    text-align: left;
  }
  
  h1, h2, h3, h4, h5 {
    font-family: 'Oswald', sans-serif;
  }
  
  a {
    color: #AF326A;
    font-weight: bold;
    text-decoration: none;

    :hover {
      text-decoration: underline;
    }
    
    :visited {
      color: #AF326A;
    }
  }

  ul {
    align-items: center;
    li {
      font-weight: 400;
    }
  }

  img {
    margin: 0px auto;
    max-height: 600px;
  }

  video {
    width: 100%;
  }
  
  blockquote {
    width: auto;
    margin: 24px auto;
    font-style: italic;
    color: #555555;
    border-left: 4px solid #000000;
    position: relative;
    background: #ededed;

    p {
      padding: 12px;
      padding-left: 24px;
      margin: 0;
    }
  }
  table {
    display: flex;
    flex-direction: column;
    thead tr {
      background: #EDEDED;
    }
    tr :last-child {
      font-weight: bold;
    }
    tr {
      display: flex;
      padding: 12px 24px;
      text-align: left;
      
      th {
        font-family: 'Open Sans', sans-serif;
        font-size: 1.3rem;
        font-weight: 600;
        letter-spacing: 2.5px;
      }
      td {
        font-family: 'Open Sans', sans-serif;
        letter-spacing: 1.5px;
      }
      td :last-child {
        margin-left: auto;
      }
    
    }
    
  }
`
export const InfoContainer = styled.div`
  display: flex;
  align-items: center;

  h4 {
    margin-right: 24px;
  }
`