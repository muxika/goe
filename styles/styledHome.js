import styled from 'styled-components';
import { fontSize, lineHeight } from '../api/api';
//  Hero section Styles
export const HeroContainer = styled.div`
  align-items: center;
  background-image: linear-gradient(rgba(0, 0, 0, .55), rgba(0, 0, 0, .55)), ${({ bg }) => `url(${bg})`};
  background-size: cover;
  display: flex;
  flex-direction: column;
  height: 100vh;
  justify-content: center;
  text-align: center;

  @media (max-width: 769px) {
    height: calc(100vw + 10vh)
  }
`
export const DonorNote = styled.div`
  background-color: rgb(173, 53, 109);
  color: white;
  display: flex;
  font-family: "Open Sans", sans-serif;
  margin-left: 70vw;
  padding: 24px;
  position: absolute;
  text-align: center;
`

export const HeroHeader = styled.h2`
  color: white;
  font-family: Oswald;
  font-weight: 300;
  letter-spacing: .175rem;
  margin: 0px auto;
  margin-bottom: 3em;
  text-align: center;
  text-transform: uppercase;
  width: 60%;

  font-size: ${fontSize(14, 26)};
  line-height: ${lineHeight};
`

//  DonationContainer section Styles
export const Appeal = styled.div`
  align-self: flex-start;
  color: white;
  font-family: Open Sans;
  font-size: ${fontSize(16, 19)};
  font-weight: 35px;
  margin-bottom: 76px;
  max-width: 650px;

  div p {
    :first-child {
      margin-top: 0px;
    }
    margin-top: 35px;
  }
`
export const DonationContainer = styled.div`
  background-image: linear-gradient(rgba(0, 0, 0, .55), rgba(0, 0, 0, .55)), ${({ bg }) => `url(${bg})`};
  background-size: cover;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  padding: 95px 70px;

  h3 {
    align-self: flex-start;
    color: white;
    font-family: Oswald;
    font-size: ${fontSize(20, 30)};
    font-weight: 300;
    letter-spacing: .75px;
    line-height: ${lineHeight()};
    margin-bottom: 1em;

    @media(max-width: 800px) {
      margin-bottom: 1em;
    }
  }
  p {
    align-self: flex-start;
    color: white;
    font-family: 'Open Sans', sans-serif;
    font-size: calc(16px + (3 * ((100vw - 300px) / 1300)));
    max-width: 650px;
  }
`

export const DonateButton = styled.div`
  max-width: 300px;
  margin-top: 24px;
`

//  MissionVisionContainer section Styles
export const MissionVisionContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  -webkit-flex-wrap: wrap;
`
export const MissionVisionHeader = styled.h3`
  color: white;
  font-family: 'Oswald', sans-serif;
  font-size: 24px;
  font-weight: 300;
  letter-spacing: .56px;
  margin-bottom: 24px;
`
export const MissionVisionContent = styled.p`
  color: white;
  font-family: 'Open Sans', sans-serif;
  font-size: ${fontSize(13, 20)};
  font-weight: 300px;
  letter-spacing: .56px;
`

export const MissionVisionContentContainer = styled.div`
  background-color: ${({ bgColor }) => bgColor};
  color: #FFFFFF;
  flex: 1;
  min-width: 480px;
  padding: 25px 50px;
  @media(max-width: 769px){
    min-width: calc(100vw - 100px);
  }
`