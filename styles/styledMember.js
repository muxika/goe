import styled from 'styled-components';
import { fontSize } from '../api/api';

export const MemberContainer = styled.div`
  max-width: 80%;
  margin: 0px auto;
  padding: 4em 0;
  text-align: center;

  @media(max-width: 650px) {
    padding: 2em 0;
  }
`
export const ProfilePicture = styled.img`
  width: 450px;
  margin: 0px auto;
  
  @media( max-width: 650px) {
    width: 80%;
  }
`
export const MemberBackground = styled.div`
  font-family: 'Open Sans', sans-serif;
  font-size: 1rem;
  font-weight: 35px;
  line-height: 2rem;
  margin: 0px auto;
  text-align: left;
  max-width: 650px;
  
  @media(max-width: 650px) {
    max-width: 100%;
  }
`
export const Name = styled.h1`
  font-family: 'Oswald', sans-serif;
  font-size: 1.5rem;
  letter-spacing: 2px;
  margin-bottom: 15px;
  text-align: center;
  @media(max-width: 650px) {
    font-size: 1.375rem;
  }
`
export const RoleContainer = styled.div`
  display: flex;
  margin: 0px auto;
  text-align: center;
  align-items: center;
  justify-content: center;
  width: 450px;
  
  @media(max-width: 650px) {
    max-width: 100%;
  }
`
export const Role = styled.p`
  background-color: #FCBD55;
  color: white;
  font-family: 'Open Sans', sans-serif;
  font-size: 1rem;
  padding: 8px 25px;
  border-radius: 25px;

  :first-child {
    margin-right: 12px;
  }
`
export const PostContainer = styled.div`
  margin: 0px auto;
  margin-top: 36px;
  width: 700px;
  text-align: left;
  @media(max-width: 650px) {
    width: 100%;
  }
`
export const PostSectionTitle = styled.p`
  font-family: 'Open Sans', sans-serif;
  font-size: 20px;
  margin: 24px;
  text-align: left;
  width: 700px;

  @media(max-width: 650px) {
    margin: 24px 0px
  }
`