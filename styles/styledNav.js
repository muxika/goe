import React from 'react';
import styled from 'styled-components';
import GoeLogo from '../public/static/GoeLogo.svg'


export const NavContainer = styled.nav`
  align-self: flex-start;
  background-color: black;
  padding: 15px 48px;
  width: calc(100% - 96px);
  
  @media (orientation: portrait) {
    padding: 15px 0px 15px 0px;
    width: 100%;
  }
`
export const Navbar = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;

  ul {
    align-items: center;
    display: flex;
    justify-content: center;
    text-align: center;

    @media(max-width: 1125px ) {
      display: none;
    }

    li {
      margin-right: 24px;
      list-style-type: none;

      :last-of-type {
        margin-right: 0px;

        a {
          padding-right: 0px;
        }
      }

      a {
        color: white;
        cursor: pointer;
        font-size: calc(9px + (4 * ((100vw - 300px) / 1300)));
        font-family: 'Open Sans', sans-serif;
        font-weight: 400;
        letter-spacing: 5.45px;
        text-transform: uppercase;
        padding: .5rem .75rem;
        text-decoration: none;

        @media(max-width: 1216px) {
          margin-right: 0px;
        }    
      }
    }
  }
`;

export const LogoContainer = styled.div`
  display: flex;
  cursor: pointer;

  a {
    color: white;
    font-family: 'Oswald', sans-serif;
    font-size: 16px;
    font-weight: 400;
    letter-spacing: 3.89px;
    line-height: 24px;
    margin-left: 1em;
    max-width: 70px;
    text-decoration: none;
    padding: 0;
  }

  @media (max-width: 936px) {
    padding-left: 24px;
  }
`;

export const Logo = styled(GoeLogo)`
  align-self: center;
`;

export const DonateButton = styled.button`
  align-items: center;
  background-color: #FCBD55;
  border-radius: 25px;
  border: none;
  display: flex;
  font-family: 'Open Sans', sans-serif;
  justify-content: center;
  padding: 1.2em;


    color: white;
    font-size: calc(10px + (3 * ((100vw - 300px) / 1300)));
    font-family: 'Open Sans', sans-serif;
    font-weight: bold;
    letter-spacing: 3.93px;
    text-transform: uppercase;
    text-decoration: none;
    margin: 0px 1em;
    margin-left: 50px;
`;
export const Last = styled.div`
  display: flex;
  
  @media (max-width: 1120px) {
    display: none;
  }
`
export const MobileLast = styled.div`
  display: none;
  
  @media(max-width: 1120px ) {
    display: inline-flex;
  }
`
export const HamburgerContainer = styled.div`
  a {
    color: white;
    font-family: 'Oswald', sans-serif;
    font-size: 16px;
    font-weight: 400;
    letter-spacing: 3.89px;
    line-height: 24px;
    margin-bottom: 24px;
    text-decoration: none;
    padding: 0;
  }
`
export const MobileLinks = styled.div`
  align-items: center;
  background-color: black;
  color: white;
  display: flex;
  flex-direction: column;
  text-align: center;
  width: 100%;
  z-index: 1;

  a {
    color: white;
    font-family: "Oswald", sans-serif;
    margin-top: 15px;
    padding: 15px;
    text-decoration: none;
    width: 100%;

    :hover {
      background: #FCBD55;
    }
  }
`

export const ButtonContainer = styled.div`
  margin: 24px auto;
  width: 162px;
`