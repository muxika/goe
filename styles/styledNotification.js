import React from 'react';
import styled from 'styled-components';
import HasNotificationBell from '../public/static/HasNotificationBell.svg';
import NoNotificationBell from '../public/static/NoNotificationBell.svg';
import { fontSize } from '../api/api';

export const NotificationContainer = styled.div`
  align-self: center;
  color: white;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
`
const icon = `
  align-self: center;
  fill: blue;
  margin: 0px 48px;
  margin-left: 0px;
  padding: 0px;
  height: 36px;
  width: 36px;
  
  path {
    fill: #FCBD55;
  }
  @media (max-width: 954px) {
    margin: 0px 24px;
  }
  @media(max-width: 890px) {
    height: 28px;
    margin: 0px 12px;
    width: 26px;
  }
`
export const NotificationIcon = styled(HasNotificationBell)`
  ${icon}
`
export const NoNotificationIcon = styled(NoNotificationBell)`
  ${icon}
`
export const ModalContainer = styled.div`
  background: black;
  box-shadow: 0px 3px 5px -1px rgba(0,0,0,0.2), 0px 6px 10px 0px rgba(0,0,0,0.14), 0px 1px 18px 0px rgba(0,0,0,0.12);
  color: white;
  display: flex;
  flex-direction: column;
  left: auto;
  width: 700px;
  position: absolute;
  right: 0;
  text-align: left;
  top: 80px;
  z-index: 1400;

  *, *::before, *::after {
    box-sizing: inherit;
  }

  @media(max-width: 890px) {
    right: 0;
    width: 100%;
  }
`
export const MarkAsRead = styled.p`
  color: #FCBD55;
  font-family: 'Open Sans', sans-serif;
  font-weight: bold;
  letter-spacing: 1.5px;
  font-size: .75rem;
  margin-bottom: 0px;
  padding: 0px 24px;
  text-align: right;
  text-decoration: underline;

  :hover {
    color: #FCBD55;
  }
`