import styled from 'styled-components';
import { fontSize } from '../api/api';

export const MemberContainer = styled.div`
  border-radius: 4px;
  box-shadow: 0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12);
  cursor: pointer;
  display: flex;
  flex-direction: column;
  margin: 1em;
  overflow: hidden;
  padding:0px;
  text-align: center;
  width: 20%;
  max-height: 422px;

  @media (max-width: 768px) {
    width: 80%;
  }
`
export const ProfilePicture = styled.img`
  width: 100%;
  object-fit: cover;
  height: 350px;
`
export const InfoContainer = styled.div`
  align-items: center;
  background-color: #FCBD55;
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  justify-content: space-evenly;
  padding: 10px 5px;
`
export const Header = styled.h2`
  align-self: center;
  color: #111;
  font-family: 'Oswald', sans-serif;
  font-size: ${fontSize(15, 16)};
  letter-spacing: 1.5px;
  text-align: center;
  margin: 0px;
  margin-bottom: 10px;

  @media (min-width: 769px) and (max-width: 1250px) {
    font-size: ${fontSize(15, 18)};
  }
`

export const Span = styled.span`
  align-self: center;
  color: #111;
  font-family: 'Open Sans', sans-serif;
  font-size: 1rem;
  letter-spacing: 2px;
  text-align: center;

  @media (min-width: 769px) and (max-width: 1250px) {
    font-size: .75rem;
    letter-spacing: 1px;
  }
`
