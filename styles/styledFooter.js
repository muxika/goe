import styled from 'styled-components';
import { fontSize } from '../api/api';


export const Foot = styled.div`
  /* bottom: 0;
  position: absolute; */
  width: 100%;
  align-self: flex-end;
`
export const FooterContainer = styled.div`
  align-items: center;
  background-color: black;
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 36px;

  @media(max-width: 500px) {
    flex-direction: row;
    padding: 9px;
    justify-content: space-evenly;
  }
`
export const FooterLinkContainer = styled.div`
  display: flex;
  justify-content: space-around;
  margin-bottom: 25px;

  ul {
    display: flex;
    justify-content: space-between;
    text-align: center;

    li {
      margin-right: 14px;
      list-style-type: none;

      a {
        color: white;
        cursor: pointer;
        font-size: calc(9px + (4 * ((100vw - 300px) / 1300)));
        font-family: 'Open Sans', sans-serif;
        font-weight: 400;
        letter-spacing: 5.45px;
        text-transform: uppercase;
        padding: .5rem .75rem;
        text-decoration: none;

        @media(max-width: 1216px) {
          margin-right: 0px;
        }
        @media(max-width: 500px) {
          padding: 0px;
        }
      }
      @media(max-width: 500px) {
        margin: .25em 0px;
        
      }
    }
    @media(max-width: 500px) {
      flex-direction: column;
      text-align: left;
      padding-left: 0px;
    }
  }
`
export const FooterLink = styled.a`
  color: white;
  font-family: 'Open Sans',  sans-serif;
  font-size: ${fontSize(8, 12)};
  font-weight: 400;
  letter-spacing: 5.45px;
  text-decoration: none;
  text-transform: uppercase;
  margin-right: 10px;
  
  &:nth-last-child(){
    margin-right: 0px;
  }

  &:focus, &:hover, &:visited, &:link, &:active {
    text-decoration: none;
  }
`
export const Copyright = styled.p`
  color: white;
  font-family: 'Open Sans', sans-serif;
  font-size: 11px;
  font-weight: 400;
  letter-spacing: 4.09px;
`

export const ContentfulLogo = styled.img`
  margin-left: auto;
  width: 110px;
`
