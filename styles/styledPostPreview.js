import styled from 'styled-components';
import { fontSize } from '../api/api';

export const ButtonContainer = styled.div`
  display: flex;
  align-items: center;
  margin-left: auto;

  @media(max-width: 500px) {
    margin-left: ${({ fromBlogPage }) => fromBlogPage && 0};
    margin-top: ${({ fromBlogPage }) => fromBlogPage && '12px'};
  }
`
export const Description = styled.p`
  font-family: 'Open Sans', sans-serif;
  font-size: ${fontSize(15, 18)};
  font-weight: 300;
  letter-spacing: 2px;
  margin: 0px;
  height: ${({ fromBlogPage }) => !fromBlogPage ? '25px' : 'auto'};
  display: ${({ fromBlogPage }) => fromBlogPage ? 'block' : 'none'};

  @media(max-width: 500px) {
    display: none;
  }
`
export const DetailsContainer = styled.div`
  /* align-self: center;
  max-width: ${({ fromBlogPage }) => fromBlogPage ? '800px' : '650px'};
  text-align: left;
  
  @media(max-width: 842px) {
    max-width: ${({ fromBlogPage }) => fromBlogPage ? '450px' : '100%'};
  } */
`
export const Name = styled.p`
  cursor: pointer;
  font-family: 'Open Sans', sans-serif;
  font-size: ${({ fromBlogPage }) => fromBlogPage ? '1rem' : '.75rem'};
  font-weight: 300;
  letter-spacing: 2px;
  margin: 15px 0px;
  text-align: left;

  :hover {
    color: #FCBD55;
    text-decoration: underline;
  }

  @media(max-width: 500px) {
    text-align: ${({ fromBlogPage }) => fromBlogPage && 'center'};
  }
`
export const PreviewContainer = styled.div`
  align-items: center;
  background-image: ${({ postBg }) => postBg === undefined ? `linear-gradient(rgba(0, 0, 0, 0.55), rgba(0, 0, 0, 0.55)), url(/static/defaultBG.jpg)` : `linear-gradient(rgba(0, 0, 0, 0.55), rgba(0, 0, 0, 0.55)), url(${postBg})`};
  background-repeat: no-repeat;
  background-size: cover;
  border-radius: 10px;
  box-shadow: 0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12);
  color: white;
  display: flex;
  margin: ${({ fromBlogPage }) => fromBlogPage ? '24px auto' : '24px'};
  padding: ${({ fromBlogPage }) => fromBlogPage ? '2em 4em' : '.5em 1em'};
  width: ${({ fromBlogPage }) => !fromBlogPage ? 'auto' : '80%'};
  
  @media(max-width: 500px) {
    flex-direction: ${({ fromBlogPage  }) =>  fromBlogPage && 'column'};
    margin: 12px auto;
    text-align: ${({ fromBlogPage }) => fromBlogPage && 'center'};
    width: ${({ fromBlogPage }) => fromBlogPage && '65%'};
  }
`
export const Tag = styled.div`
  align-items: center;
  background-color: #FCBD55;
  border: none;
  border-radius: 16px;
  box-sizing: border-box;
  color: black;
  cursor: default;
  display: inline-flex;
  font-family: 'Open Sans', sans-serif;
  font-size: 0.8125rem;
  height: 32px;
  justify-content: center;
  margin-right: 12px;
  outline: none;
  padding: 0;
  text-decoration: none;
  vertical-align: middle;
  white-space: nowrap;
`
export const TagContainer = styled.div`
  display: ${({ fromBlogPage }) => fromBlogPage ? 'flex' : 'none'};
  padding: 0px;
  margin: 20px 0px;

  @media(max-width: 500px) {
    display: none;
  }
`
export const TagLabel = styled.span`
  cursor: inherit;
  display: flex;
  align-items: center;
  user-select: none;
  white-space: nowrap;
  padding-left: 12px;
  padding-right: 12px;
`
export const TitleLink = styled.p`
  cursor: pointer;
  font-family: 'Oswald', sans-serif;
  font-weight: 300;
  font-size:   ${({ fromBlogPage }) => fromBlogPage ? '1.5rem' : '1rem'};
  letter-spacing: 2.3px;
  margin: 0px;
  margin-top: ${({ fromBlogPage }) => fromBlogPage ? '0' : '16px'};

  @media(max-width: 500px){
    font-size: ${({ fromBlogPage }) => fromBlogPage && '1.25rem'};
    line-height: ${({ fromBlogPage }) => fromBlogPage && '2rem'};
  }
  :hover {
    color: #FCBD55;
    text-decoration: underline;
  }
`;
