import styled from 'styled-components';

export const ButtonContainer = styled.a`
  align-items: center;
  background-color: #FCBD55;
  border-radius: 22px;
  border: none;
  cursor: pointer;
  display: flex;
  justify-content: center;
  padding: 1em;
  text-decoration: none;
  text-transform: uppercase;
  margin: 0px;
  
  @media(max-width: 500px) {
    padding: .5em;
    min-width: 140px;
    margin: 0px;
  }
`

export const InnerButton = styled.span`
  color: black;
  font-size: .75rem;
  font-family: "Open Sans", sans-serif;
  font-weight: bold;
  letter-spacing: 3.93px;
`