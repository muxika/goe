import styled from 'styled-components';

export const ChildContainer = styled.div`
  align-self: center;
  flex-grow: 1;
  flex: 1;
  width: 100%;
`

export const Container  = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0px;
  min-height: 100vh;
  position: relative;
`