import styled from 'styled-components';
import { fontSize, lineHeight } from '../api/api';

export const AboutContainer = styled.div`
  margin: 0px auto;
  margin-bottom: 48px;
  width: 80%;

  h2 {
    color: #222222;
    font-family: 'Oswald';
    font-size: 3rem;
    font-weight: 300;
    letter-spacing: 4px;
    margin: 24px auto;
  }

  p {
    font-family: 'Open Sans', sans-serif;
    font-size: ${fontSize(14, 18)};
    line-height: ${lineHeight()};
    font-weight: 400;
    letter-spacing: .75px;

    display: block;
    margin-top: 1em;
    margin-bottom: 1em;
    margin-left: 0;
    margin-right: 0;
    text-align: left;
  }
  
  img {
    margin-bottom: 24px;
    object-fit: cover;
    width: 100%;
  }
`