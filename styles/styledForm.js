import React from 'react';
import styled from 'styled-components';
import { fontSize, lineHeight } from '../api/api';
import GoeLogo from '../public/static/GoeLogo.svg'

export const ThankYouContainer = styled.div`
  text-align: center;
  width: 80%;
  height: 100%;
  margin: 0px auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 15%;

  h1 {
    font-family: 'Open Sans', sans-serif;
  }
`
export const ThankYouLogoContainer = styled.div`
  /* text-align: center;
  display: block;
  margin: 0px auto; */
`
export const ThankYouLogo = styled(GoeLogo)`
  align-self: center;
  width: 400px;
  height: 400px;
`;

export const StyledForm = styled.div`
  margin: 0px auto;
  margin-bottom: 48px;
  width: 80%;
  
  h1, h2, h4, h5 {
    font-family: 'Oswald', sans-serif;
  }

  h3 {
    font-family: 'Open Sans', sans-serif;
  }

  h2 {
    color: #222222;
    font-family: 'Oswald', sans-serif;
    font-size: 3rem;
    font-weight: 300;
    letter-spacing: 4px;
    margin: 48px auto;
  }

  p {
    font-family: 'Open Sans', sans-serif;
    font-size: ${fontSize(14, 18)};
    line-height: ${lineHeight()};
    font-weight: 400;
    letter-spacing: .75px;

    display: block;
    margin-top: 1em;
    margin-bottom: 1em;
    margin-left: 0;
    margin-right: 0;
    text-align: left;
  }
  
  img {
    margin-bottom: 24px;
    object-fit: cover;
    width: 100%;
  }

  form {
    display: flex;
    flex-direction: column;
    margin-top: 48px;

    textarea:focus, input:focus{
      outline: none;
      border-bottom: 2px solid black;
    }
    *:focus {
      outline: none;
    }
    label{
      font-family: 'Open Sans', sans-serif;
      margin-top: 24px;
      margin-bottom: 12px;
    }
    p {
      margin: 0px;
      margin-top: 12px;
      margin-bottom: 24px;
    }
    input {
      border: 0px;
      border-bottom: 1px solid black;
      font-family: "Open Sans", sans-serif;
    }
    

    h3, input {
      margin-bottom: 24px;
    }

    select {
      margin-bottom: 48px;
    }

    button {
      align-items: center;
      background-color: #FCBD55;
      border-radius: 22px;
      border: none;
      cursor: pointer;
      display: inline-flex;
      justify-content: center;
      padding: 1em 2em;
      text-decoration: none;
      text-transform: uppercase;
      margin: 0px;

      span {
        color: black;
        font-size: .75rem;
        font-family: "Open Sans", sans-serif;
        font-weight: bold;
        letter-spacing: 3px;
      }
    }
  }
`

export const DetailsContainer = styled.div`
  margin: 48px auto;
  h3 {
    font-family: "Open Sans", sans-serif;
    margin-top: 60px;
    margin-bottom: 24px;
  }
`

export const Checkboxes = styled.div`
  font-family: "Open Sans", sans-serif;
  margin-top: 24px;
  div {
    margin-right: 24px;

    input[type='checkbox'] {
      margin-right: 12px;
      background-color:#000;
      border-left-color:#06F;
      border-right-color:#06F;
    }
  }
`