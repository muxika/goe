import styled from 'styled-components';

// MemberSection Styles
export const SectionTitle = styled.p`
  color: black;
  cursor: pointer;
  font-family: 'Oswald', sans-serif;
  font-size: 26px;
  letter-spacing: .175rem;
  line-height: 36px;
  margin: 48px auto;
  text-align: center;
`
// MemberNav Styles
export const GroupContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 2em auto;
`
export const LinksContainer = styled.ul`
  align-items: center;
  display: flex;
  justify-content: space-between;
  text-align: center;
  padding: 0px 15px;

  li:first-child {
    /* margin-left: 0px; */
  }
  li:last-child {
    /* margin-right: 0px; */
  }
  li {
    list-style: none;
    width: 100%;
    /* margin: 0px 48px; */

    @media(max-width: 1135px) {
      /* text-align: center; */
      /* margin: 0px 24px; */
    }
    @media(max-width: 729px) {
      /* text-align: center; */
      /* margin: 0px 9px; */
    }
  }
`
export const MemberPageLink = styled.a`
  cursor: pointer;
  font-family: 'Oswald', sans-serif;
  font-size: 1.375rem;
  letter-spacing: .175rem;
  line-height: 2rem;
  text-decoration: ${({ active }) => active && 'underline'};

  color: ${({ active }) => active ? 'black' : '#999999'};

  font-weight: ${({ active }) => active && 'bold'};

  :hover {
    color: black;
    text-decoration: underline;
  }
  @media(max-width: 729px) {
    font-size: .875rem;
    line-height: 23px;
  }
`