const homeQuery = `{
  homePageContentCollection {
    items {
      heroImage {
        url
      }
      heroTagline
      missionStatement
      visionStatement
      donationAppeal
      donationAppealBackgroundImage {
        url
      }
    }
  }
}`

export default homeQuery;