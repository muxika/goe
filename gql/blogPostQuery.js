const blogPostQuery = /* GraphQL */`
  query getBlogPost($title: String!){
    blogPostCollection(where: { title_contains: $title}) {
      items{
        title
        author {
          name
        }
        body
        dateTime
        postBannerImage {
          url
        }
        tags
      }
    }
  }
`

export default blogPostQuery;