const alertQuery = /* GraphQL */`{
  updatesAlertCollection(where: { alertEnabled: true }) {
    items {
      sys {
        id
      }
      contentReference {
        ...on BlogPost {          
          author {
            name
          }
          description
          postBannerImage {
            url
          }
          postType
          sys {
            id
          }
          title
        }
        ...on OrganizationMember{
          name
        }
      }
      nonContentRelatedAlert
    }
  }
}`

export default alertQuery;