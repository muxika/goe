const aboutPageQuery = `{
  aboutPageContentCollection {
    items {
      aboutUsContent 
    }
  }
}`

export default aboutPageQuery;