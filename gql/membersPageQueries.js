export const teamQuery = `{
  organizationMemberCollection(where: {memberRole_contains_some: ["Team"]}) {
    items {
      name
      profilePicture {
        url
      }
      memberLocation
      memberRole
      teamRole
      sys {
        id
      }
    }
  }
}`
export const instructorAndAssistantQuery = `{
  organizationMemberCollection(where:{memberRole_contains_some: ["Instructor", "Administrative Assistant", "Teaching Assistant"] }) {
    items {
      name
      profilePicture {
        url
      }
      memberLocation
      memberRole
      sys {
        id
      }
    }
  }
}`
export const partnerAndSponsorQuery = `{
  organizationMemberCollection(where: { partnerOrSponsor: true }) {
    items {
      name
      profilePicture {
        url
      }
      memberLocation
      memberRole
      partnersAndSponsorsRole
      sys {
        id
      }
    }
  }
}`
export const executiveBoardQuery =`{
  organizationMemberCollection(where: { memberOfExecutiveBoard: true }) {
    items {
      name
      profilePicture {
        url
      }
      executiveBoardRole
      memberLocation
      memberRole
      sys {
        id
      }
    }
  }
}`
