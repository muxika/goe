
const projectPostQuery = /* GraphQL */`
  query getProjectPost($title: String!){
    projectPostCollection(where: { title_contains: $title}) {
      items{
        title
        author {
          name
        }
        body
        dateTime
        postBannerImage {
          url
        }
        tags
      }
    }
  }
`

export default projectPostQuery;