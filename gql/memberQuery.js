const memberQuery = /* GraphQL */ `
  query getMember($name: String!) {
    organizationMemberCollection(where: { name_contains: $name }){
      items {
        aboutMember {
          json
        }
        name
        profilePicture {
          url
          description
        }
        executiveBoardRole
        instructorRole
        memberLocation
        memberRole
        partnersAndSponsorsRole
        postsCollection(limit: 30) {
          ...on OrganizationMemberPostsCollection {
            items {
              description
              postBannerImage {
                url
              }
              sys {
                id
              }
              title
            }
          }
        }
        teamRole
      }
    }
  }
`

export default memberQuery;
