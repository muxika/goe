const blogPostPreviewQuery = `
  {
    blogPostCollection(where: { postType: "Blog" }) {
      items {
        author {
          name
          sys {
            id
          }
        }
        dateTime
        description
        sys {
          id
        }
        postBannerImage {
          url
        }
        tags
        title
      }
    }
  }
`

export default blogPostPreviewQuery;