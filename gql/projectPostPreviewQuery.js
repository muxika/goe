const projectPostPreviewQuery = `
  {
    blogPostCollection(where: { postType: "Project" }) {
      items {
        author {
          name
          sys {
            id
          }
        }
        dateTime
        description
        sys {
          id
        }
        postBannerImage {
          url
        }
        tags
        title
      }
    }
  }
`

export default projectPostPreviewQuery;