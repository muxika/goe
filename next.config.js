const withGraphql = require('next-plugin-graphql');
const withImages = require('next-images');

const nextConfig = {
  env: {
    CONTENTFUL_SPACE_ID: process.env.CONTENTFUL_SPACE_ID,
    CONTENTFUL_ACCESS_TOKEN: process.env.CONTENTFUL_ACCESS_TOKEN,
    ANNUAL_FUNDRAISER_KEY: process.env.ANNUAL_FUNDRAISER_KEY,
    ANNUAL_FUNDRAISER_BASE: process.env.ANNUAL_FUNDRAISER_BASE,
    GIVE_EDUCATION_KEY: process.env.GIVE_EDUCATION_KEY,
    GIVE_EDUCATION_BASE: process.env.GIVE_EDUCATION_BASE
  },
};

module.exports = withGraphql(withImages(nextConfig));