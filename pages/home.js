import { useEffect } from 'react'
import ReactLoading from 'react-loading'
import { useStateValue, resetNotifications } from '../state'
import { gqlQuery } from '../api/api'
import homeQuery from '../gql/homeQuery';
import HeroSection from '../components/HeroSection';
import DonationSection from '../components/DonationSection';
import MissionVisionSection from '../components/MissionVisionSection';
import { DonorNote } from '../styles/styledHome';
const Home = () => {
  const [{ home, hasReviewedNotification, loading }, dispatch] = useStateValue();
  
  useEffect(() => {
    dispatch({ loading: true })
    gqlQuery(homeQuery)
      .then(({ homePageContentCollection }) =>
        dispatch(
          {
            home: homePageContentCollection.items[0],
            ...resetNotifications(hasReviewedNotification),
            loading: false
          }
        )
      );
  }, []);

  return (
    <>
      {loading ? 
        <ReactLoading type="bubbles" color="#FCBD55" height={667} width={375} /> 
      : home && (
        <>
          <DonorNote>100% of your donation will go to GOE's online teaching program as well as providing aid to internally displaced persons (IDPs)</DonorNote>
          <HeroSection
            buttonText="LEARN MORE ABOUT US"
            heroBg={home.heroImage.url}
            tagline={home.heroTagline}
          />
          <MissionVisionSection
            mission={home.missionStatement}
            vision={home.visionStatement}
          />
          <DonationSection
            bg={home.donationAppealBackgroundImage.url}
            buttonText="DONATE NOW"
            appeal={home.donationAppeal}
          />
        </>
      )
      }
    </>
  )
}

export default Home