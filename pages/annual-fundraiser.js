import { useEffect, useState } from 'react';
import Airtable from 'airtable';
import { useStateValue, resetNotifications } from '../state'
import { Checkboxes, FundraiserDetails, StyledForm, ThankYouLogoContainer, ThankYouLogo, ThankYouContainer } from '../styles/styledForm';

Airtable.configure({
  endpointUrl: 'https://api.airtable.com',
  apiKey: process.env.ANNUAL_FUNDRAISER_KEY
});

const base = Airtable.base(process.env.ANNUAL_FUNDRAISER_BASE);

const GiveEducation = () => {
  const [name, setName] = useState("")
  const [phone, setPhone] = useState("")
  const [email, setEmail] = useState("")
  const [attendance, setAttendance] = useState(false)
  const [submittedForm, setSubmittedForm] = useState(false)

  const [{ hasReviewedNotification }, dispatch] = useStateValue();

  useEffect(() => dispatch(
    {
      ...resetNotifications(hasReviewedNotification),
    }
  ), [dispatch]);

  const formData = {
    fields: {
      name, phone, email,
      attendance: attendance ? 'I will attend' : 'I will not attend'
    }
  }

  const handleSubmit = () => base("2020").create(
    [ formData ],
    function(err, records) {
      if (err) {
        console.error(err)
        return;
      }
      setSubmittedForm(true)
      setTimeout(function () {
        window.location.href = window.location.href;
      }, 10000)
    });
  
  return (
      submittedForm ?

        <ThankYouContainer>
          <h1>Thank you, we look forward to your company on the 22nd</h1>
          <ThankYouLogoContainer>
            <ThankYouLogo />
          </ThankYouLogoContainer>
          
        </ThankYouContainer>
      
      :

        <StyledForm>
          <h2>Gift of Education</h2>
          <h3>2020 Annual Fundraiser</h3>

          <form onSubmit={(e) => {
            e.preventDefault()
            handleSubmit()
          }}>
            <h3>RSVP</h3>

            <label>Full Name</label>
            <input type="text" name="name" value={name} required onChange={e => setName(e.target.value)} />

            <label>Contact Phone</label>
            <input type="phone" name="phone" value={phone} required onChange={e => setPhone(e.target.value)} />

            <label>Email Address</label>
            <input type="email" name="email" value={email} required onChange={e => setEmail(e.target.value.replace(/\s/g, ''))} />

            <Checkboxes>
              <div>
                <input type="radio" name="attendance" value={attendance} onChange={e => setAttendance(true)} /> 
                I will attend
              </div>
              <div>
                <input type="radio" name="attendance" value={attendance} onChange={e => setAttendance(false)} /> 
                  I will not attend
              </div>
            </Checkboxes>
            <span>
              <button type="submit">
                <span>Submit</span>
              </button>
            </span>
          </form>
        </StyledForm>
  )
}

export default GiveEducation