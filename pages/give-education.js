import { useEffect, useState } from 'react';
import Airtable from 'airtable';
import { useStateValue, resetNotifications } from '../state'
import { Checkboxes, DetailsContainer, StyledForm, ThankYouLogoContainer, ThankYouLogo, ThankYouContainer } from '../styles/styledForm';

Airtable.configure({
  endpointUrl: 'https://api.airtable.com',
  apiKey: process.env.GIVE_EDUCATION_KEY
});

const base = Airtable.base(process.env.GIVE_EDUCATION_BASE);

const GiveEducation = () => {
  const [donation, setDonation ] = useState('')
  const [name, setName] = useState("")
  const [phone, setPhone] = useState("")
  const [email, setEmail] = useState("")
  const [attendance, setAttendance] = useState(false)
  const [submittedForm, setSubmittedForm] = useState(false)

  const [{ hasReviewedNotification }, dispatch] = useStateValue();

  useEffect(() => dispatch(
    {
      ...resetNotifications(hasReviewedNotification),
    }
  ), [dispatch]);

  const formData = {
    fields: {
      Donation: parseFloat(donation.replace(/[^0-9.]/g, "")),
      Name: name,
      Phone: phone,
      Email: email,
      Attendance: attendance ? 'I will attend' : 'I will NOT attend'
    }
  }

  const handleSubmit = () => base("Give Education Fundraiser").create(
    [ formData ],
    function(err, records) {
      if (err) {
        console.error(err)
        return;
      }
      setSubmittedForm(true)
      setTimeout(function () {
        window.location.href = window.location.href;
      }, 10000)
    });
  
  return (
      submittedForm ?

        <ThankYouContainer>
          <h1>Thank you for your contribution to GOE's Professional Teacher Development Program</h1>
          <ThankYouLogoContainer>
            <ThankYouLogo />
          </ThankYouLogoContainer>
          
        </ThankYouContainer>
      
      :

        <StyledForm>
          <h2>A Gift to Education</h2>
          <h3>Support our program and help us create a better future for Myanmar's children!</h3>
          <DetailsContainer>
            <h3>Gift of Education Professional Teacher Development Program</h3>
            <p><strong>When:</strong> April 27 to May 29 2020 (5 Weeks)</p>
            <p><strong>Where:</strong> Moe Thauk Pann Academic Center, Yangon</p>
            <p><strong>Our Instructors:</strong> 5 International Instructors &amp; 4 Local Instructors</p>
            <p><strong>Teacher Trainees:</strong> 60 teachers from across Myanmar</p>
          </DetailsContainer>

        <p>Thank you for your interest in Gift of Education’s 7th year of Professional Teacher Development Program. Your donation will provide training over five weeks for 60 teachers from across Myanmar. On behalf of our teacher trainees and the thousands of students who will benefit from your donation, we would like to thank you.</p>

          <form onSubmit={(e) => {
            e.preventDefault()
            handleSubmit()
          }}>
            <h3>HOW YOU CAN CONTRIBUTE TO GOE'S PROFESSIONAL TEACHER DEVELOPMENT PROGRAM:</h3>

            <label>Donation Amount (MMK)</label>
            <input 
              type="string"
              name="donation"
              value={donation} 
              onChange={e => setDonation(e.target.value.replace(/[^0-9.,]/g, "").replace(/,{2,}/g, ','))}
            />

            <label>Full Name</label>
            <input type="text" name="name" value={name} required onChange={e => setName(e.target.value)} />

            <label>Contact Phone</label>
            <input type="phone" name="phone" value={phone} required onChange={e => setPhone(e.target.value)} />

            <label>Email Address</label>
            <input type="email" name="email" value={email} required onChange={e => setEmail(e.target.value.replace(/\s/g, ''))} />

            <Checkboxes>
              <div>
                <input type="radio" name="attendance" value={attendance} onChange={e => setAttendance(true)} /> I will attend "The Art of Diplomacy and Entertaining"
                </div>
              <div>
                <input type="radio" name="attendance" value={attendance} onChange={e => setAttendance(false)} /> I will NOT attend "The Art of Diplomacy and Entertaining"
                </div>
            </Checkboxes>

            <p><i>Due to limited seating, we regret to let you know that only the invitee will be allowed to attend the event.</i></p>
            <span>
              <button type="submit">
                <span>Submit</span>
              </button>
            </span>
          </form>
        </StyledForm>
  )
}

export default GiveEducation