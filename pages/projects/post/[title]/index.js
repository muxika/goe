import { withRouter } from 'next/router';
import projectPostQuery from '../../../../gql/projectPostQuery';
import SinglePost from '../../../../components/SinglePost';

const ProjectPost = ({ router }) => {
  const title = router.asPath.substring('/project/post/'.length).replace(/-/g, ' ')
  return <SinglePost  title={title} query={projectPostQuery} />
};

export default withRouter(ProjectPost);


