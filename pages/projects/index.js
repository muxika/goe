import React from 'react';
import projectPostPreviewQuery from '../../gql/projectPostPreviewQuery';
import PostIndex from '../../components/PostIndex';


const Blog = () => <PostIndex query={projectPostPreviewQuery} />;


export default Blog;
