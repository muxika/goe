import { useEffect } from 'react';
import ReactLoading from 'react-loading';
import { useStateValue, resetNotifications } from '../../../state'
import { gqlQuery } from '../../../api/api';
import Members from '../../../components/Members';
import MembersPageWrapper from '../../../components/MembersPageWrapper';
import { executiveBoardQuery } from '../../../gql/membersPageQueries';

const ExecutiveBoard = () => {
  const [{ members, hasReviewedNotification, loading }, dispatch] = useStateValue();

  useEffect(() => {
    dispatch({ memberView: 'executiveBoard', loading: true });
    gqlQuery(executiveBoardQuery).then(response => 
      dispatch({ 
        members: response.organizationMemberCollection.items,
        ...resetNotifications(hasReviewedNotification),
        loading: false
      })
    );
  }, [dispatch]);

  return (
    <MembersPageWrapper>
      {loading ?
        <ReactLoading type="bubbles" color="#FCBD55" height={667} width={375} />
      : members && <Members members={members} />
      }
    </MembersPageWrapper>
  );
};

export default ExecutiveBoard;
