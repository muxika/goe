import { useEffect } from 'react';
import ReactLoading from 'react-loading';
import { useStateValue, resetNotifications } from '../../../state';
import { gqlQuery } from '../../../api/api';
import Members from '../../../components/Members';
import MembersPageWrapper from '../../../components/MembersPageWrapper';
import { teamQuery } from '../../../gql/membersPageQueries';

const Team = () => {
  const [{ members, hasReviewedNotification, loading }, dispatch] = useStateValue();

  useEffect(() => {
    dispatch({ memberView: 'team', loading: true })
    gqlQuery(teamQuery).then(response => dispatch({
      members: response.organizationMemberCollection.items,
      ...resetNotifications(hasReviewedNotification),
      loading: false
    }))
  }, [dispatch]);

  const executives = members && members.filter(m => m.name === 'Sunda Khin' || m.name === 'Susan K. Coti');
  const restOfTeam = members && members.filter(m => m.name !== 'Sunda Khin' && m.name !== 'Susan K. Coti');
  const sortedMembers = members && [...executives, ...restOfTeam];
  
  return (
    <MembersPageWrapper>
      {loading ?
        <ReactLoading type="bubbles" color="#FCBD55" height={667} width={375} />
      : members && <Members members={sortedMembers} />
      }
    </MembersPageWrapper>
  );
};

export default Team;
