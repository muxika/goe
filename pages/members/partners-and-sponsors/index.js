import { useEffect } from 'react';
import ReactLoading from 'react-loading';
import { useStateValue, resetNotifications } from '../../../state'
import { fieldFilter, gqlQuery } from '../../../api/api';
import MemberSection from '../../../components/MemberSection';
import MembersPageWrapper from '../../../components/MembersPageWrapper';
import { partnerAndSponsorQuery } from '../../../gql/membersPageQueries';


const PartnersAndSponsors = () => {
  const [{ members, hasReviewedNotification, loading }, dispatch] = useStateValue();

  useEffect(() => {
    dispatch({ memberView: 'partnersAndSponsors', loading: true});
    gqlQuery(partnerAndSponsorQuery).then(response =>
      dispatch({ 
        members: response.organizationMemberCollection.items,
        ...resetNotifications(hasReviewedNotification),
        loading: false
      })
    );
  }, [dispatch]);

  const typeLocFilter = (type, location) => {
    const arr = fieldFilter(members, 'partnersAndSponsorsRole', type)
    const locations = {
      usa: fieldFilter(arr, 'partnersAndSponsorsRole', 'USA'),
      myanmar: fieldFilter(arr, 'partnersAndSponsorsRole', 'Myanmar')
    }
    
    return locations[location]
  }

  return (
    <>
      {loading ?
        <ReactLoading type="bubbles" color="#FCBD55" height={667} width={375} />
      : members && (
          <MembersPageWrapper>
            <MemberSection sectionTitle="Patron" members={fieldFilter(members, 'partnersAndSponsorsRole', 'Patron')} />

            <MemberSection sectionTitle="Myanmar Sponsors" members={typeLocFilter('Sponsor', 'myanmar')} />
            <MemberSection sectionTitle="USA Sponsors" members={typeLocFilter('Sponsor', 'usa')} />

            <MemberSection sectionTitle="Myanmar Partners" members={typeLocFilter('Partner', 'myanmar')} />
            <MemberSection sectionTitle="USA Partners" members={typeLocFilter('Partner', 'usa')} />
          </MembersPageWrapper>
        )
      }
    </>
  )
};

export default PartnersAndSponsors;
