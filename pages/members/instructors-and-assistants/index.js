import React, { useEffect } from 'react';
import ReactLoading from 'react-loading';
import { useStateValue, resetNotifications } from '../../../state'
import { fieldFilter, gqlQuery, groupBy } from '../../../api/api';
import MembersPageWrapper from '../../../components/MembersPageWrapper';
import MemberSection from '../../../components/MemberSection';
import { instructorAndAssistantQuery, instructorAndInternQuery } from '../../../gql/membersPageQueries';

const Instructors = () => {
  const [{ members, hasReviewedNotification, loading }, dispatch] = useStateValue();

  useEffect(() => {
    dispatch({ memberView: 'instructorsAndAssistants', loading: true });
    gqlQuery(instructorAndAssistantQuery).then(response =>
      dispatch({ 
        members: response.organizationMemberCollection.items,
        ...resetNotifications(hasReviewedNotification),
        loading: false
      })
    );
  }, [dispatch]);

  const renderMembers = () => {

    const groupedByLoc = groupBy(members, "memberLocation");
    
    const filterRole = (arr, role) => arr.filter(m => m.memberRole.includes(role));

    const priority = {
      "USA": 1,
      "Myanmar": 2,
      "Australia": 3,
      "France": 4
    }
    // Sort by region, then priority. 
    const sortedByRegion = Object.entries(groupedByLoc).map(([key, value]) =>  ({ [key]: value, "priority": priority[key] }) ).sort( (a,b) => a.priority - b.priority )

    return members && sortedByRegion.map( region => {
      const key = Object.keys(region)[0]
      return (
        <div key={key}>
          <MemberSection sectionTitle={`${key} Instructors`} members={filterRole(region[key], "Instructor")} />
          <MemberSection sectionTitle={`${key} Teaching Assistants`} members={filterRole(region[key], "Teaching Assistant")} />
          <MemberSection sectionTitle={`${key} Administrative Assistants`} members={filterRole(region[key], "Administrative Assistant")} />
        </div>
      )
    })
  }

  return (
    <MembersPageWrapper>
      {loading ? 
        <ReactLoading type="bubbles" color="#FCBD55" height={667} width={375} />
        : members && renderMembers() }
    </MembersPageWrapper>
  )
  
};

export default Instructors;
