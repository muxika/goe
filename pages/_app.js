import App from 'next/app'
import React from 'react'
import { ThemeProvider } from 'styled-components'
import { Normalize } from 'styled-normalize'
import { StateProvider, initialState, reducer } from '../state'
import Layout from '../components/Layout'

const theme = {
  colors: {
    primary: '#0070f3'
  }
}

export default class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props
    return (
      <ThemeProvider theme={theme}>
        <Normalize />
        <StateProvider initialState={initialState} reducer={reducer}>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </StateProvider>
      </ThemeProvider>
    )
  }
}