import { useEffect } from 'react';
import { withRouter } from 'next/router';
import ReactLoading from 'react-loading';
import { useStateValue, resetNotifications } from '../../../state'
import { gqlQuery } from '../../../api/api';
import Member from '../../../components/Member';
import memberQuery, { postsByMemberQuery } from '../../../gql/memberQuery';


const MemberPage = ({ router }) => {
  const name = router.asPath.substring('/member/'.length).replace(/-/g, ' ')
  // For now, the use of 'name' causes hot module reloading to fail.
  // Temporary workaround: Change 'name' to 'nom'
  
  const [{ member, hasReviewedNotification, loading }, dispatch] = useStateValue();
  useEffect(() => {
    dispatch({ loading: true })
    gqlQuery(memberQuery, { name })
      .then(response => dispatch({
        member: response.organizationMemberCollection.items[0],
        ...resetNotifications(hasReviewedNotification),
        loading: false
      }))
  }, []);
  
  return (
    <>
      {loading ? 
        <ReactLoading type="bubbles" color="#FCBD55" height={667} width={375} />
      : member && <Member m={member} />
      }
    </>
  );
};

export default withRouter(MemberPage);
