import { useEffect } from 'react';
import ReactMarkdown from 'react-markdown';
import remarkGfm from 'remark-gfm';
import remarkRehype from 'remark-rehype';
import ReactLoading from 'react-loading';
import { gqlQuery } from '../api/api';
import { useStateValue, resetNotifications } from '../state'
import { AboutContainer } from '../styles/styledAbout';
import aboutPageQuery from '../gql/aboutPageQuery';

const About = () => {
  const [{ about, hasReviewedNotification, loading }, dispatch] = useStateValue();

  useEffect(() => {
    dispatch({ loading: true })
    gqlQuery(aboutPageQuery).then(({ aboutPageContentCollection }) =>
      dispatch(
        {
          about: aboutPageContentCollection.items[0].aboutUsContent,
          ...resetNotifications(hasReviewedNotification),
          loading: false
        }
      )
    );
  }, [dispatch]);

  return (
    <>
      {loading ? 
        <ReactLoading type="bubbles" color="#FCBD55" height={667} width={375} />
      : about && (
          <AboutContainer>
            <ReactMarkdown children={about} remarkPlugins={[remarkGfm, remarkRehype]} />
          </AboutContainer>
        )
      }
    </>
  );
};

export default About;
