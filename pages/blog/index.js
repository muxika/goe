import React from 'react';
import blogPostPreviewQuery from '../../gql/blogPostPreviewQuery';
import PostIndex from '../../components/PostIndex';


const Blog = () => <PostIndex query={blogPostPreviewQuery} />;


export default Blog;
