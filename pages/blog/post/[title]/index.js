import { withRouter } from 'next/router';
import blogPostQuery from '../../../../gql/blogPostQuery';
import SinglePost from '../../../../components/SinglePost';

const BlogPost = ({ router }) => {
  const title = router.asPath.substring('/blog/post/'.length).replace(/-/g, ' ')
  return <SinglePost title={title} query={blogPostQuery} />
};

export default withRouter(BlogPost);


