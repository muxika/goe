import { createClient } from 'contentful';
import { request } from 'graphql-request';
// const space = process.env.SPACE
// console.log(space)
// const accessToken = process.env.ACCESS_TOKEN

const base = 'https://cdn.contentful.com'
const space = `${process.env.CONTENTFUL_SPACE_ID}`
const accessToken = `${process.env.CONTENTFUL_ACCESS_TOKEN}`

export const url = `https://graphql.contentful.com/content/v1/spaces/${space}?access_token=${accessToken}`

export const gqlQuery = (query, variables) => request(url, query, variables)
  .then(data => data)
  .catch(console.error)

export const roles = [
  'instructorRole',
  'teamRole',
  'partnersAndSponsorsRole',
  'executiveBoardRole'
]

const setEntryType = type => {
  const CONTENT_TYPE_IDS = {
    about: 'aboutPageContent',
    authors: 'author',
    blog: 'blogPost',
    faces: 'facesOfGoe',
    home: 'homePageContent',
    members: 'members',
    org: 'organizationMember',
    updates: 'updatesAlert',
  };

  return { content_type: CONTENT_TYPE_IDS[type] };
};

export const client = createClient({ space, accessToken });

export const fetchAllEntries = () =>
  client.getEntries().then(entries => {
    entries.items.forEach(entry => {
      if (entry.fields) {
        return entry.fields;
      }
    });
  });

export const filterResponse = (response, filterParam) =>
  response.items.filter(e => e.sys.contentType.sys.id === filterParam);

export const filterPageData = filterParam =>
  client.getEntries().then(response => filterResponse(response, filterParam));

export const memberFilter = filterBy =>
  filterPageData('organizationMember', 'members').then(response =>
    response.filter(r => r.fields.orgSection.includes(filterBy))
  );

export const formatStringForDynamicRoute = string =>
  string
    .replace(/[.,/#!$%^&*;:{}=_`~()]/g, '')
    .replace(/\s+/g, '-')
    .toLowerCase();

export const getContentByType = type =>
  client.getEntries(setEntryType(type)).then(response => response);

export const filterEntries = (contentType, fieldName, filterBy) =>
  getContentByType(contentType).then(response =>
    response.items.filter(r => r.fields[fieldName] && r.fields[fieldName].includes(filterBy))
  );

export const fieldFilter = (array, fieldName, filterBy) => array.filter(r => r[fieldName] && r[fieldName].includes(filterBy))

export const fontSize = (size, max) => `calc(${size}px + (${max} - ${size}) * ((100vw - 300px) / 1300))`

export const lineHeight = () => `calc(2em + 0.3 * ((100vw - 300px) / (1300)))`


export const groupBy = (arr, property) => {
  return arr.reduce((acc, cur) => {
    acc[cur[property]] = [...acc[cur[property]] || [], cur];
    return acc;
  }, {});
}
