const routes = [
  {
    label: 'Home',
    path: '/',
  },
  {
    label: 'About',
    path: '/about',
  },
  {
    label: 'Members',
    path: '/members',
  },
  {
    label: 'Projects',
    path: '/projects',
    as: '/projects'
  },
  {
    label: 'Blog',
    path: '/blog',
    as: '/blog'
  }
]

export default routes;