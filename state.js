import React, { createContext, useContext, useReducer } from 'react';
import PropTypes from 'prop-types';

// Set any initial state
export const initialState = {
  about: null,
  blog: null,
  hamburgerOpen: false,
  hasReviewedNotification: false,
  home: null,
  loading: true,
  members: null,
  member: null,
  memberView: '',
  notification: null,
  notificationModalOpen: false,
  post: null,
  resp: null,
  viewport: null
};

// State Context
export const StateContext = createContext();
// State Provider
export const StateProvider = ({ reducer, initialState, children }) => (
  <StateContext.Provider value={useReducer(reducer, initialState)}>
    {children}
  </StateContext.Provider>
);
// Validate Props
StateProvider.propTypes = {
  children: PropTypes.node.isRequired,
  initialState: PropTypes.object.isRequired,
  reducer: PropTypes.func.isRequired,
};
// Export ability to manage state within provider/context with dispatch
export const useStateValue = () => useContext(StateContext);
// Export State Reducer
export const reducer = (state, action) => action ? { ...state, ...action } : state;

// Re-usable actions
export const resetNotifications = (reviewed) => ({ 
  notificationModalOpen: false,
  hasReviewedNotification: reviewed ? true : false
});

